#include <cstdio>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main()
{
	string word;
	int wordCount = 1;

	while( cin >> word ) {
		long long total = 0;

		for( int i = 0 ; i < word.length(); i++ ) {
			if( word[i] == 'b' ) {
				total += pow( 2, (word.length() - 1 - i) );
			}
		}

		if( wordCount != 1 ) {
			cout << endl << endl;
		}

		cout << "Palavra " << wordCount << endl;
		cout << total;

		wordCount++;
	}

        return 0;
}
