#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <climits>
#include <utility>

using namespace std;

class Graph {
	vector<vector<pair<int, int> > > adj;
	int size;

	public:
	Graph(int n) {
		size = n;
		adj.resize(n);
	}

	void clear() {
		adj.clear();
		size = 0;
	}

	void addEdge( int n1, int n2, int weight ) {
		adj[n1].push_back( make_pair(weight, n2) );
		adj[n2].push_back( make_pair(weight, n1) );
	}

	int dijkstra( int src, int dst ) {
		priority_queue<pair<int, int>, vector<pair<int, int> >, std::greater<pair<int, int> > > priorQueue;
		vector<int> dist;
		vector<bool> visited;

		visited.resize( size, 0 );
		dist.resize( size, INT_MAX );

		visited[src] = true;

		priorQueue.push( make_pair(0, src) );

		while( !priorQueue.empty() ) {
			int dist_u = priorQueue.top().first;
			int u = priorQueue.top().second;
			priorQueue.pop();

			visited[u] = true;

			if( u == dst ) {
				 break;
			}

			for( int i = 0; i < adj[u].size(); i++) {
				int weight = adj[u][i].first;
				int v = adj[u][i].second;
				int dist_through_u = dist_u + weight;

				if( !visited[v] ) {
					if( dist[v] > dist_through_u ) {
						dist[v] = dist_through_u;
						priorQueue.push( make_pair(dist[v], v) );
					}
				}
			}
		}

		return dist[dst];
	}
};

int main() {
	int N, M;
	cin >> N >> M;

	Graph g(N+2);

	for( ; M > 0; M-- ) {
		int n1, n2, weight;
		cin >> n1 >> n2 >> weight;
		
		g.addEdge( n1, n2, weight );
	}

	cout << g.dijkstra( 0, N+1 ) << endl;
	g.clear();

	return 0;
}
