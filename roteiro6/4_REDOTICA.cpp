#include <iostream>
#include <vector>
#include <string>
#include <list>
 
#include <limits> // for numeric_limits
 
#include <set>
#include <utility> // for pair
#include <algorithm>
#include <iterator>
 
using namespace std;
 
typedef int vertex_t;
typedef double weight_t;
 
const weight_t max_weight = numeric_limits<double>::infinity();
 
struct neighbor {
    vertex_t target;
    weight_t weight;
    neighbor(vertex_t arg_target, weight_t arg_weight)
        : target(arg_target), weight(arg_weight) { }
};
 
typedef vector<vector<neighbor> > adj_list_t;
 
 
void prim(vertex_t source, const adj_list_t &adj_list, vector<weight_t> &min_distance, vector<vertex_t> &previous)
{
    int n = adj_list.size();
    min_distance.clear();
    min_distance.resize(n, max_weight);
    min_distance[source] = 0;
    previous.clear();
    previous.resize(n, -1);
    set<pair<weight_t, vertex_t> > vertex_queue;
    vertex_queue.insert(make_pair(min_distance[source], source));
    list<int> nodes;
 
    while (!vertex_queue.empty()) 
    {
        weight_t dist = vertex_queue.begin()->first;
        vertex_t u = vertex_queue.begin()->second;
        vertex_queue.erase(vertex_queue.begin());
	nodes.push_back(u);
 
        if( u != source ) {
	    if( previous[u] < u ) {
		cout << previous[u]+1 << " " << u+1 << endl;
	    }
	    else {
		cout << u+1 << " " << previous[u]+1 << endl;
	    }
	}

        // Visit each edge exiting u
	const vector<neighbor> &neighbors = adj_list[u];
        for (vector<neighbor>::const_iterator nb_it = neighbors.begin(); nb_it != neighbors.end(); nb_it++)
        {
            vertex_t v = nb_it->target;
            weight_t weight = nb_it->weight;

	    if( (find(nodes.begin(), nodes.end(), v) == nodes.end()) && weight < min_distance[v] ) {
	        vertex_queue.erase(make_pair(min_distance[v], v));
	        min_distance[v] = weight;
 		previous[v] = u;
	        vertex_queue.insert(make_pair(min_distance[v], v));
	    }
 
        }
    }
}
 
int main()
{
	int N, M;
	int test = 1;

	while( (cin >> N >> M) && (N != 0) ) {
		adj_list_t adj_list(N);

		for( int i = 0; i < M; i++ ) {
			int n1, n2, weight;
			cin >> n1 >> n2 >> weight;
			adj_list[n1-1].push_back(neighbor(n2-1, weight));
			adj_list[n2-1].push_back(neighbor(n1-1, weight));
		}

		vector<weight_t> min_distance;
		vector<vertex_t> previous;

		cout << "Teste " << test << endl;
		prim(0, adj_list, min_distance, previous);
		cout << endl;

		test++;
	}

	return 0;
}
