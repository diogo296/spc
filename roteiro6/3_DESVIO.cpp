#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <climits>
#include <utility>

using namespace std;

class Graph {
	vector<vector<pair<int, int> > > adj;
	int size;

	public:
	Graph(int n) {
		size = n;
		adj.resize(n);
	}

	void clear() {
		adj.clear();
		size = 0;
	}

	void addEdge( int n1, int n2, int weight ) {
		adj[n1].push_back( make_pair(weight, n2) );
	}

	int dijkstra( int src, int dst ) {
		priority_queue<pair<int, int>, vector<pair<int, int> >, std::greater<pair<int, int> > > priorQueue;
		vector<int> dist;
		vector<bool> visited;

		visited.resize( size, 0 );
		dist.resize( size, INT_MAX );

		visited[src] = true;

		priorQueue.push( make_pair(0, src) );

		while( !priorQueue.empty() ) {
			int dist_u = priorQueue.top().first;
			int u = priorQueue.top().second;
			priorQueue.pop();

			visited[u] = true;

			if( u == dst ) {
				 break;
			}

			for( int i = 0; i < adj[u].size(); i++) {
				int weight = adj[u][i].first;
				int v = adj[u][i].second;
				int dist_through_u = dist_u + weight;

				if( !visited[v] ) {
					if( dist[v] > dist_through_u ) {
						dist[v] = dist_through_u;
						priorQueue.push( make_pair(dist[v], v) );
					}
				}
			}
		}

		return dist[dst];
	}
};

int main() {
	int n, m, c, k, u, v, p;

	while( (cin >> n >> m >> c >> k) && (n != 0) ) {
		Graph g(n);

		for( ; m > 0; m-- ) {
			cin >> u >> v >> p;

			if( (u < c) && (v < c) ) {
				if( u == v-1 ) {
					g.addEdge( u, v, p );
				}

				if (v == u-1) {
					g.addEdge( v, u, p );
				}
			}
			else {
				if( (u < c) && (v >= c) ) {
					g.addEdge( v, u, p );
				}
				else if( (v < c) && (u >= c) ) {
					g.addEdge( u, v, p );
				}
				else if( (u >= c) && (v >= c) ) {
					g.addEdge( u, v, p );
					g.addEdge( v, u, p );
				}
			}
		}

		cout << g.dijkstra( k, c-1 ) << endl;
		g.clear();
	}	

	return 0;
}
