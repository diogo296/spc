#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

#define MAX 250*250 + 10

using namespace std;

int main(){
	int b[MAX], c[MAX];
	int T, N, P, Q, cases = 1;

	cin >> T;

	while( T-- ) {
		int pos = 0;
		memset( c, 0, sizeof c );

		cin >> N >> P >> Q;

		for( int i = 1; i <= P+1; i++) {
			cin >> N;
			c[N] = i;
		}
		
		for( int i = 1; i <= Q+1; i++ ) {
			cin >> N;

			if( c[N] ) {
				b[pos++] = c[N];
			}
		}

		vector <int> d(pos+1);
		d[0] = -INT_MAX;

		for( int i = 1; i <= pos; i++) {
			d[i] = INT_MAX;
		}

		int subseqMax = 0, j;

		for( int i = 0; i < pos; i++) {
			j = (int) ( upper_bound(d.begin(), d.end(), b[i]) - d.begin() );

			if( d[j-1] < b[i] && b[i] < d[j] ) {
				d[j] = b[i];
			}

			if( subseqMax < j ) {
				subseqMax = j;
			}
		}

		cout << "Case " << cases++ << ": " << subseqMax << endl;
	}

	return 0;
}
