#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

typedef struct { 
	double x;
	double y; 
} Point;

double dist( Point p1, Point p2 ) {
	return hypot(p1.x - p2.x, p1.y - p2.y);
}

bool circle2PtsRad( Point p1, Point p2, double r, Point *c ) {
	double d2 = (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
	double det = r*r / d2 - 0.25;

	if( det < 0) {
		return false;
	}

	double h = sqrt(det);

	c->x = (p1.x + p2.x) * 0.5 + (p1.y - p2.y) * h;
	c->y = (p1.y + p2.y) * 0.5 + (p2.x - p1.x) * h;

	return true;
}

int main() {
	int N;

	while( (cin >> N) && (N != 0) ) {
		double R;
		Point p1;
		vector <Point> points(N);

		for( int i = 0; i < N; i++ ) {
			cin >> points[i].x >> points[i].y;
		}

		cin >> R;

		bool canPack = false;
		int k;

		for( int i = 0; i < N; i++ ) {
			for( int j = 0; j < N; j++ ) {
				if( i != j ) {
					circle2PtsRad( points[i], points[j], R, &p1 );

					for( k = 0; k < N; k++ ) {
						if( dist(points[k],p1) > R ) {
							break;
						}
					}

					if( k == N ) {
						canPack = true;
					}
				}
			}
		}

		if( canPack ) {
			cout << "The polygon can be packed in the circle." << endl;
		}
		else {
	   		cout << "There is no way of packing that polygon." << endl;
		}
	}

	return 0;
}
