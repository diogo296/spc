#include <iostream>
#include <cmath>

using namespace std;

int main() {
	double D, Vf, Vg;

	while( cin >> D >> Vf >> Vg ) {
		if( 12 / Vf >= hypot(12, D) / Vg ) {
			cout << "S" << endl;
		}
		else {
			cout << "N" << endl;
		}
	}

    return 0;
}
