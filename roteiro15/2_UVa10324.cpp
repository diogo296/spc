#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>

#define N 1000000

using namespace std;


int main ()
{
	char str[N+5];
	int array[N+5], cases = 1;

	while( gets(str) ) {
		int len = strlen(str);

		if( len == 0 ) {
			break;
		}

		if( str[0] == '1' ) {
			array[0] = 1;
		}
		else {
			array[0] = 0;
		}

		for( int i = 1; i < len; i++ ) {
			array[i] = array[i-1];

			if( str[i] == '1' ) {
				array[i]++;
			}
		}

		int queries;
		cin >> queries;

		cout << "Case " << cases++ << ":" << endl;

		while( queries-- ) {
			int i, j;
			cin >> i >> j;

			if( i > j ) {
			   	swap(i, j);
			}

			if( (str[i] == '0' && str[j] == '0' && array[j] - array[i] == 0) ||
				(str[i] == '1' && str[j] == '1' && array[j] - array[i] == j - i) ) {
				cout << "Yes" << endl;
			}
			else {
				cout << "No" << endl;
			}
		}

		getchar ();
	}

	return 0;
}
