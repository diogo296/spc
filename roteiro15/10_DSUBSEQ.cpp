#include <iostream>

#define MOD 1000000007
#define N 1048576

using namespace std;

int main() {
	char str[N];
	int array[N], last[128], i, T;
   
	cin >> T;
	array[0] = 1;

	while( T-- ) {
		for( int j = 0; j < 128; j++ ) {
			last[j] = 0;
		}

		cin >> str + 1;

		for( i = 1; str[i]; i++ ) {
			array[i] = array[i-1] << 1;

			if( last[str[i]] ) {
				array[i] -= array[last[str[i]] - 1];
			}

			if( array[i] < 0 ) {
				array[i] += MOD;
			}
			else if( array[i] >= MOD ) {
				array[i] -= MOD;
			}

			last[str[i]] = i;
		}

		cout << array[i-1] << endl;
	}

	return 0;
}
