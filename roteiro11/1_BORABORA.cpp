#include <iostream>
#include <algorithm>
#include <list>
#include <vector>

#define vc vector<carta> 

using namespace std;
struct carta {
	int num;
	char naipe;
};

bool cmp( carta a, carta b ) {
	return( (a.num > b.num) || (a.num == b.num && a.naipe > b.naipe) );
}

int main() {
	int P, M, N, i, j, r, s, dir, jogAtual;
	char c;
	carta cd, d, cartaTopo;

	while( (cin >> P >> M >> N) && (P + M + N != 0) ) {
		vector<vc> jogadores(P);

		for( i = 0; i < P; i++ ) {
			for( j = 0; j < M; j++ ) {
				cin >> cd.num >> cd.naipe;
				jogadores[i].push_back(cd);
			}

			sort( jogadores[i].begin(), jogadores[i].end(), cmp );
		}

		cin >> cartaTopo.num >> cartaTopo.naipe;

		list<carta> baralho;

		for( i = 1; i < N-(P*M); i++ ) {
			cin >> cd.num >> cd.naipe;
			baralho.push_front(cd); 
		}

		int turn = 1;
		jogAtual = P-1; 
		dir = 1;
		bool descartado = true;

		if( cartaTopo.num == 12 ) { 
			jogAtual = 1;
			dir = -1;
		}

		while( true ) {
			jogAtual = jogAtual + dir;

			if( jogAtual == -1 ) {
			   	jogAtual = (P-1);
			}
			if( jogAtual == P ) {
			   	jogAtual = 0;
			}

			if( (cartaTopo.num == 7) && descartado ) {
				descartado = false;

				if( baralho.size() > 0 ) {
					d = baralho.back(); 
					baralho.pop_back();
					jogadores[jogAtual].push_back(d);
				}
				if( baralho.size() > 0 ) {
					d = baralho.back(); 
					baralho.pop_back();
					jogadores[jogAtual].push_back(d);
				}

				sort( jogadores[jogAtual].begin(), jogadores[jogAtual].end(), cmp );
				continue;
			} 
			if( (cartaTopo.num == 1) && descartado) {
				descartado = false;

				if( baralho.size() > 0 ) {
					d = baralho.back(); 
					baralho.pop_back();
					jogadores[jogAtual].push_back(d);
				}

				sort( jogadores[jogAtual].begin(), jogadores[jogAtual].end(), cmp );
				continue;
			} 
			if( (cartaTopo.num == 11) && descartado) {
				descartado = false;
				continue;
			} 

			bool descartar = false; 

			for( vc::iterator it = jogadores[jogAtual].begin(); it != jogadores[jogAtual].end(); it++ ) {
				if( (*it).num == cartaTopo.num || (*it).naipe == cartaTopo.naipe) {
					descartado = true;
					cartaTopo = *it;
					jogadores[jogAtual].erase(it);
					descartar = true;

					break;
				}
			}

			if( !descartar ) {
				if( baralho.size() > 0 ) {
					d = baralho.back();
					baralho.pop_back();

					if( (d.num == cartaTopo.num) || (d.naipe == cartaTopo.naipe) ) {
						descartado = true;
						cartaTopo = d;
					} 
					else {
						jogadores[jogAtual].push_back(d);
						sort( jogadores[jogAtual].begin(), jogadores[jogAtual].end(), cmp );
					}
				}
			}
			if( (cartaTopo.num == 12) && descartado) {
				descartado = false;
				dir = ( (dir == 1)? -1:1 );
			}
			if( jogadores[jogAtual].size() == 0 ) {
				cout << (jogAtual + 1) << endl;
				break;
			}
		} 
	}
	return 0;
}
