#include <stdio.h> 
#include <ctype.h> 
#include <queue> 

using namespace std; 

int main() { 
	int n; 

	while( scanf("%d", &n) == 1 && (n != 0) ) { 
		double profit = 0.0; 
		priority_queue<float> purchases; 
		priority_queue<float> sales; 

		for( ; n > 0; n-- ) { 
			char proposal; 
			float value; 
			scanf("\n%c %f", &proposal, &value);

			if( tolower(proposal)=='c' ) {
				if( !sales.empty() ) { 
					float diff = value + sales.top(); 

					if( diff >= 0.0 ) { 
						profit += diff; 
						sales.pop(); 
					} 
					else { 
						purchases.push(value); 
					} 
				} 
				else { 
					purchases.push(value); 
				} 
			} 
			else {
				if( !purchases.empty() ) {
					float diff = purchases.top() - value;

					if( diff >= 0.0 ) {
						profit += diff; 
						purchases.pop(); 
					} 
					else { 
						sales.push(-value); 
					} 
				} 
				else { 
					sales.push(-value); 
				} 
			} 
		} 

		printf( "%.2f\n", profit ); 
	} 

	return 0; 
} 
