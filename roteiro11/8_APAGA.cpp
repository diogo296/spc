#include <iostream>
#include <deque>

using namespace std;

int main () {
	int N, D;
	char number[100002];

	while( (cin >> N >> D) && (N != 0) ) {
		deque<char> dq;
		int limit = N - D;

		cin >> number;

		for( int i = 0; i < N; i++ ) {
			while( !dq.empty() && number[i] > dq.back() && D > 0 ) {
				dq.pop_back();
				D--;
			}

			if( dq.size() < limit ) {
				dq.push_back( number[i] );
			}
		}

		for( deque<char>::iterator it = dq.begin(); it != dq.end(); it++ ) {
			cout << *it;
		}

		cout << endl;

		dq.clear();
	}

	return 0;
}
