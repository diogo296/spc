#include <stdio.h>
#include <stdlib.h>

int main()
{
	int rodadas, figVirada, aldo, beto;
	int nTeste = 1, i;

	scanf("%d", &rodadas);

	while( rodadas != 0 )
	{
		aldo = 0;
		beto = 0;
	
		for( i = 0; i < rodadas; i++ )
		{
			scanf("%d", &figVirada);
			aldo += figVirada;
			
			scanf("%d", &figVirada);
			beto += figVirada;
		}
		
		printf( "Teste %d\n", nTeste );
		
		if( aldo > beto )
		{
			printf( "Aldo\n\n" );
		}
		else
		{
			printf( "Beto\n\n" );
		}
		
		scanf("%d", &rodadas);
		nTeste++;
	}

	return EXIT_SUCCESS;
}