#include <stdio.h>
#include <stdlib.h>

int main()
{
	int numLinhas = 0, numColunas = 0, max = 0;
	int minhocas[100][100], total = 0;
	int i = 0, j = 0;

	scanf("%d", &numLinhas);
	scanf("%d", &numColunas);

	for( i = 0; i < numLinhas; i++ )
	{
		total = 0;

		for( j = 0; j < numColunas; j++ )
		{
			scanf("%d", &minhocas[i][j]);

			total += minhocas[i][j];
		}

		if( total > max )
		{
			max = total;
		}
	}

	for( j = 0; j < numColunas; j++ )
	{
		total = 0;

		for( i = 0; i < numLinhas; i++ )
		{
			total += minhocas[i][j];
		}

		if( total > max )
		{
			max = total;
		}
	}

	printf("%d", max);

	return EXIT_SUCCESS;
}