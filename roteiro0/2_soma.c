#include <stdio.h>
#include <stdlib.h>

int main()
{
	int numInteiros = 0, valor = 0, total = 0, i = 0;

	scanf("%d", &numInteiros);

	for( i = 0; i < numInteiros; i++ )
	{
		scanf("%d", &valor);

		total += valor;
	}

	printf("%d", total);

	return EXIT_SUCCESS;
}