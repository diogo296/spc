#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int postosAgua, distMax;
	int p1, p2;
	const int DIST_MARATONA = 42195;
	bool fail = false;

	cin >> postosAgua >> distMax;
	cin >> p1;

	for( int i = 1; i < postosAgua; i++ )
	{
		cin >> p2;

		if( p2 - p1 > distMax )
		{
			fail = true;

			break;
		}

		p1 = p2;
	}

	if( (DIST_MARATONA - p2 <= distMax) && !fail )
	{
		cout << "S" << endl;
	}
	else
	{
		cout << "N" << endl;
	}

        return 0;
}
