#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	int fifty;
	int ten;
	int five;
	int one;
} Bills;

int main()
{
	int value, count = 1;
	Bills bills;

	scanf("%d", &value);

	while( value != 0 )
	{
		bills.fifty = 0;
		bills.ten = 0;
		bills.five = 0;
		bills.one = 0;

		while( value > 0 )
		{
			if( value >= 50 )
			{
				bills.fifty++;
				value -= 50;
			}
			else if( value >= 10 )
			{
				bills.ten++;
				value -= 10;
			}
			else if( value >= 5 )
			{
				bills.five++;
				value -= 5;
			}
			else
			{
				bills.one++;
				value--;
			}
		}

		printf( "Teste %d\n", count );
		printf( "%d %d %d %d\n", bills.fifty, bills.ten, bills.five, bills.one );

		count++;

		scanf("%d", &value);
	}

	return EXIT_SUCCESS;
}
