#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int N;
	
	cin >> N;

	while( N != 0 )
	{
		int vetor[N];

		for( int i = 0; i < N; i++ )
		{
			cin >> vetor[i];
		}

		int maxAtual = 0, maxTotal = -1;

		for( int i = 0; i < N; i++ )
		{
			maxAtual += vetor[i];

			if( maxAtual < 0 )
			{ 
				maxAtual = 0;
			}
			if( maxAtual > maxTotal )
			{
				maxTotal = maxAtual;
			} 
		}

		if( maxTotal > 0 )
		{
			cout << "The maximum winning streak is " << maxTotal << "." << endl;
		}
		else
		{
			cout << "Losing streak." << endl;
		}

		cin >> N;
	}

        return 0;
}
