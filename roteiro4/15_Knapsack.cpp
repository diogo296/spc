#include <cstdio>
#include <iostream>

using namespace std;

int max( int a, int b ) {
	if( a > b ) {
		return a;
	} 
	else {
		return b;
	}
}

int main() {
	int bagCap, numItens;

	cin >> bagCap >> numItens;

	int values[numItens], sizes[numItens];

	for( int i = 0; i < numItens; i++ ) {
		cin >> sizes[i] >> values[i];
	}

	int bag[numItens+1][bagCap+1];

	for( int i = 0; i <= bagCap; i++ ) {
		bag[0][i] = 0;
	}

	for( int i = 1; i <= numItens; i++ ) {
		for( int j = 0; j <= bagCap; j++ ) {
			if( sizes[i-1] <= j ) {
				bag[i][j] = max( bag[i-1][j], bag[i-1][j - sizes[i-1] ] + values[i-1] );
			}
			else {
				bag[i][j] = bag[i-1][j];
			}
		}
	}
	cout << bag[numItens][bagCap] << endl;

        return 0;
}
