#include <cstdio>
#include <iostream>

using namespace std;

int max( int a, int b ) {
	if( a > b ) {
		return a;
	} 
	else {
		return b;
	}
}

int knapsack( int bagCap, int numItens, int values[], int sizes[] ) {
	int bag[numItens+1][bagCap+1];

	for( int i = 0; i <= bagCap; i++ ) {
		bag[0][i] = 0;
	}

	for( int i = 1; i <= numItens; i++ ) {
		for( int j = 0; j <= bagCap; j++ ) {
			if( sizes[i-1] <= j ) {
				bag[i][j] = max( bag[i-1][j], bag[i-1][j - sizes[i-1] ] + values[i-1] );
			}
			else {
				bag[i][j] = bag[i-1][j];
			}
		}
	}

	return bag[numItens][bagCap];
}

int main() {
	int bagCap, numItens;
	int count = 1;

	cin >> bagCap >> numItens;

	while( (bagCap != 0) && (numItens != 0) ) {
		int values[numItens], sizes[numItens];

		for( int i = 0; i < numItens; i++ ) {
			cin >> sizes[i] >> values[i];
		}

		cout << "Teste " << count << endl;
		cout << knapsack( bagCap, numItens, values, sizes ) << endl << endl;

		cin >> bagCap >> numItens;
		count++;
	}

        return 0;
}
