#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int N;

	cin >> N;

	while( N != 0 ) {
		bool line[N][N], column[N][N];
		int rooks = 0;
		char position;

		for( int i = 0; i < N; i++ ) {
			for( int j = 0; j < N; j++ ) {
				cin >> position;

				bool lineUp = true;
				bool lastCol = true;

				if( i > 0 ) {
					lineUp = line[i-1][j];
				}
				if( j > 0 ) {
					lastCol = column[i][j-1];
				}

				if( position == '.' ) {
					if( lineUp && lastCol ) {
						line[i][j] = false;
						column[i][j] = false;
						rooks++;
					}
					else { 
						line[i][j] = lineUp;
						column[i][j] = true;
					}
				}
				else {
					line[i][j] = true;
					column[i][j] = true;
				}

			}
		}

		cout << rooks;
		cin >> N;

		if( N != 0 ) {
			cout << endl;
		}
	}

        return 0;
}
