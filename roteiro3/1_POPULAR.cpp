#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int numAlunos, voto;

	cin >> numAlunos;

	while( numAlunos != 0 ) {
		int alunos[numAlunos];

		for( int i = 0; i < numAlunos; i++ ) {
			alunos[i] = 0;
		}

		for( int i = 0; i < numAlunos; i++ ) {
			for( int j = 0; j < numAlunos; j++ ) {
				cin >> voto;
				alunos[j] += voto;	
			}
		}

		int maior = -1;

		for( int i = 0; i < numAlunos; i++ ) {
			if( alunos[i] > maior ) {
				maior = alunos[i];
			}
		}

		cout << maior << endl;
		cin >> numAlunos;
	}

        return 0;
}
