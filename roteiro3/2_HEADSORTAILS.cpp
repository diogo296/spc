#include <cstdio>
#include <iostream>
#include <queue>
#include <sstream>

using namespace std;

int main()
{
	int maxA, maxB, minA, minB;
	queue <string> combinations;

	cin >> maxA >> maxB >> minA >> minB;

	while( minA <= minB ) {
		minA++;
	}

	for( int i = minA; i <= maxA; i++ ) {
		for( int j = minB; (j < i) && (j <= maxB); j++ ) {
			std::ostringstream comb;
			comb << i << " " << j;

			combinations.push( comb.str() );
		}
	}

	cout << combinations.size() << endl;

	while( !combinations.empty() ) {
		cout << combinations.front() << endl;
		combinations.pop();
	}

        return 0;
}
