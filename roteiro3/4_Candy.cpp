#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int numPackets;

	cin >> numPackets;

	while( numPackets != -1 ) {
		int packets[numPackets], total = 0;

		for( int i = 0; i < numPackets; i++ ) {
			cin >> packets[i];
			total += packets[i];
		}

		int movedCandies = -1;

		if( total % numPackets == 0 ) {
			movedCandies = 0;

			for( int i = 0; i < numPackets; i++ ) {
				int diff = packets[i] - (total / numPackets);

				if( diff > 0 ) {
					movedCandies += diff;
				}
			}
		}

		cout << movedCandies << endl;
		cin >> numPackets;
	}

        return 0;
}
