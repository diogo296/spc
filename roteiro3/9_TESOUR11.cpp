#include <cstdio>
#include <iostream>
#include <cmath>

using namespace std;

typedef struct {
        int x;
        int y;
        int dist;
} Clue;

int main()
{
        int N, K;

        cin >> N >> K;

        int mat[N][N];
        Clue clues[K];

        for( int i = 0; i < K; i++ ) {
                cin >> clues[i].x >> clues[i].y >> clues[i].dist;
        }

        // init mat
        for( int i = 0; i < N; i++ ) {
                for( int j = 0; j < N; j++ ) {
                        mat[i][j] = 0;
                }
        }

        Clue best;
        best.x = best.y = best.dist = 0;

        for( int i = 0; i < N; i++ ) {
                for( int j = 0; j < N; j++ ) {
                        for( int k = 0; k < K; k++ ) {
                                Clue clue = clues[k];

                                if( clue.dist == abs(i - clue.x) + abs(j - clue.y) ) {
                                        mat[i][j]++;
                                }
                        }

                        if( best.dist < mat[i][j] ) {
                                best.x = i;
                                best.y = j;
                                best.dist = mat[i][j];
                        }
                }
        }

        for( int i = 0; i < N; i++ ) {
                for( int j = 0; j < N; j++ ) {
                       if( (mat[i][j] == best.dist) && ((best.x != i) || (best.y != j)) ) {
                               cout << "-1 -1" << endl;

                               return 0;
                       }
                }
        }

        cout << best.x << " " << best.y << endl;

        return 0;
}
