#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int numTests;
	long long polygonSize;

	cin >> numTests;

	for( int i = 0; i < numTests; i++ ) {
		cin >> polygonSize;

		if( polygonSize % 3 == 0 ) {
			cout << polygonSize / 3 << endl;
		}
		else {
			cout << 0 << endl;
		}
	}

        return 0;
}
