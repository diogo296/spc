#include <algorithm>
#include <climits>
#include <iostream>
#include <cstring>
#include <queue>
#include <vector>

using namespace std;

int mf, f = 1, S, T, C;
int res[110][110];
vector<int> p;

void augment( int v, int min_edge ) {
	if( v == S ) {
		f = min_edge;
		return;
	}
	else if( p[v] != -1 ) {
		augment( p[v], min(min_edge, res[p[v]][v]) );
		res[p[v]][v] -= f;
		res[v][p[v]] += f;
	}
}

int main() {
	int N = 27;
	S = 'A'; T = 'Z';

	memset(res, 0, sizeof(res));
	int n1, n2, bandw;

	for( cin >> C; C > 0; C-- ) {
		cin >> n1 >> n2 >> bandw;
		n1--; n2--;
		res[n1][n2] += bandw;
		res[n2][n1] += bandw;
	}

	mf = 0;

	do {
		f = 0;
		vector<int> dist(N, INT_MAX);
		dist[S] = 0;
		queue<int> q;
		q.push(S);
		p.assign(N, -1);

		while( !q.empty() ) {
			int u = q.front();
			q.pop();

			if( u == T ) {
				break;
			}

			for( int v = 0; v < N; v++ ) {
				if( (res[u][v] > 0) && (dist[v] == INT_MAX) ) {
					dist[v] = dist[u] + 1;
					q.push(v);
					p[v] = u;
				}
			}
		}

		augment( T, INT_MAX );
		mf += f;
	} while( f != 0 );

	cout << mf << endl;

	return 0;
}
