#include <iostream>

using namespace std;

int gcd( int a, int b ) {
	if( a==0 ) {
		 return b;
	}

	return gcd( b%a, a );
}

int main() {
	int N, F1, F2;

	for( cin >> N; N > 0; N-- ) {
		cin >> F1 >> F2;
		cout << gcd( F1, F2 ) << endl;
	}

        return 0;
}
