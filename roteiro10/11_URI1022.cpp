#include <iostream>
#include <cmath>

using namespace std;

int gcd( int a, int b ) {
	if( a==0 ) {
		 return b;
	}

	return gcd( b%a, a );
}

int main() {
	int N;

	for( cin >> N; N > 0; N-- ) {
		int N1, D1, N2, D2;
		int X1, Y1, X2, Y2;
		char slash, op;
		
		cin >> N1 >> slash >> D1 >> op >> N2 >> slash >> D2;

		if( op == '+' ) {
			X1 = N1*D2 + N2*D1;
			Y1 = D1*D2;
		}
		else if( op == '-' ) {
			X1 = N1*D2 - N2*D1;
			Y1 = D1*D2;
		}
		else if( op == '*' ) {
			X1 = N1*N2;
			Y1 = D1*D2;
		}
		else if( op == '/' ) {
			X1 = N1*D2;
			Y1 = N2*D1;
		}

		int div = gcd( X1, Y1 );
		X2 = X1 / div;
		Y2 = Y1 / div;

		if( (X1 < 0) || (Y1 < 0) ) {
			cout << "-"; 
		}

		cout << abs(X1) << "/" << abs(Y1) << " = ";

		if( (X2 < 0) || (Y2 < 0) ) {
			cout << "-"; 
		}

		cout << abs(X2) << "/" << abs(Y2) << endl;
	}

        return 0;
}
