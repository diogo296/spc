#include <iostream>

#define LL long long int

using namespace std;

int main() {
	LL N;

	while( (cin >> N) && (N >= 0) ) {
		cout << (N*N + N)/2 + 1 << endl;
	}

	return 0;
}
