#include <iostream>
#include <string>

using namespace std;

int charToInt( char c ) {
	return( c - '0' );
}

bool isDivBy11( string N, int i, int sumEven, int sumOdd ) {
	int len = N.length();

	if( i < len ) {
		sumEven = (sumEven + charToInt(N[i])) % 11;
	}
	if( i+1 < len ) {
		sumOdd = (sumOdd + charToInt(N[i+1])) % 11;
	}

	if( (i >= len) || (i+1 >= len) ) {
		return( sumEven == sumOdd );
	}
	else {
		return( isDivBy11(N, i+2, sumEven, sumOdd) );
	}
}

int main() {
	string N;

	while( (cin >> N) && (N != "0") ) {
		if( isDivBy11(N, 0, 0, 0) ) {
			cout << N << " is a multiple of 11." << endl;
		}
		else {
			cout << N << " is not a multiple of 11." << endl;
		}
	}

        return 0;
}
