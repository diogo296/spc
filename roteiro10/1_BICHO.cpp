#include <iostream>
#include <iomanip>

using namespace std;

int main() {
	double V;
	int N, M;

	while( (cin >> V >> N >> M) && (V != 0) ) {
		double prize;
		int n = N % 100;
		int m = M % 100;

		if( n == 0 ) {
			n = 100;
		}
		if( m == 0 ) {
			m = 100;
		}
		
		if( (N % 10000) == (M % 10000) ) {
			prize = V * 3000;
		}
		else if( (N % 1000) == (M % 1000) ) {
			prize = V * 500;
		}
		else if( (N % 100) == (M % 100) ) {
			prize = V * 50;
		}
		else if( (n-1)/4 == (m-1)/4 ) {
			prize = V * 16;
		}
		else {
			prize = 0;
		}

		cout << fixed << setprecision(2) << prize << endl;
	}

	return 0;
}
