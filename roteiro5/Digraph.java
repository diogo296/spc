import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Digraph
{
    // This is the adjacency list representation of the digraph
    // The nodes are denoted here as Integers
    // Each node is associated with a list of Integers, which indicates its out-neighbours
    protected HashMap<Integer, List<Integer>> data;

    private HashMap<Integer, Node> nodeList;

    // The collection of node in the graph
    // This set is the key set of data
    protected Set<Integer> nodeSet;
    
    private Map<Integer, Boolean> visited;
    
    public Digraph()
    {
        data = new HashMap<Integer, List<Integer>>();
        nodeList = new HashMap<Integer, Node>();
        nodeSet = data.keySet();
    }
    
    public static void main(String[] args) {
    	Digraph graph = new Digraph();
    	Scanner in = new Scanner(System.in);
    	
    	int N = in.nextInt();
    	int M = in.nextInt();
    	
    	while( N != 0 )
    	{
	    	for( int i = 1; i <= N; i++ )
	    	{
	    		graph.add(i);
	    	}
	    	
	    	for( int i = 0; i < M; i++ )
	    	{
	    		int n1 = in.nextInt();
	    		int n2 = in.nextInt();
	    		
	    		graph.addEdge(n1, n2);
	    		
	    		if( in.nextInt() == 2 )
	    		{
	    			graph.addEdge(n2, n1);
	    		}
	    	}
	    	
	    	graph.checkPath();
	    	graph.clear();
	    	
	    	N = in.nextInt();
	    	M = in.nextInt();
    	}
    }
    
    public void checkPath() {
    	for( Integer node : nodeSet )
    	{
    		if( !performDFS(node) )
    		{
    			System.out.println( "0" );
    			return;
    		}
    	}
    	
    	System.out.println( "1" );
    }
    
 // obtains the next unvisted node
    private Integer getNextUnvistedNode()
    {
        for( Integer node : visited.keySet() )
        {
            if( !visited.get( node ) )
            {
                return node;
            }
        }
        return null;
    }

    // perfoms depth first search on a digraph until all nodes are visited
    private boolean performDFS( Integer source )
    {
        visited = new HashMap<Integer, Boolean>();
        
        for( Integer node : nodeSet )
        {
            visited.put( node, Boolean.FALSE );
        }

//        while( source != null )
//        {
        recursiveDFS( source );
//            source = getNextUnvistedNode();
//        }
            
        return( getNextUnvistedNode() == null );
    }

    // depth first search from a node
    private void recursiveDFS( Integer node )
    {
        visited.put( node, Boolean.TRUE );
        
        for( Integer adjacentNode : data.get( node ) )
        {
            if( !visited.get( adjacentNode ) )
            {
                recursiveDFS( adjacentNode );
            }
        }
    }
    
    /**
     * The method adds a node to the digraph, labeled by the int value node
     *
     */
    public void add( int node )
    {
        // If the label node is already in the digraph, do nothing and return
        if( data.containsKey( (Integer) node ) )
        {
            return;
        }
        // Create a new linked list
        List<Integer> list = new LinkedList<Integer>();
        // Add a new entry to the adjacency list
        data.put( (Integer) node, list );

        // Create a new node in the GUI
        Node nodeVisual = new Node( node );
        nodeList.put( (Integer) node, nodeVisual );
    }

    /**
     * The method adds an edge to the digraph The source of the edge is labeled
     * node1 The target of the edge is labeled node2
     *
     */
    public void addEdge( int node1, int node2 )
    {
        if( node1 == node2 )
        {
            return;
        }
        if( !data.containsKey( (Integer) node1 ) || !data.containsKey( (Integer) node2 ) )
        {
            return;
        }
        List<Integer> list = data.get( (Integer) node1 );
        if( !list.contains( (Integer) node2 ) )
        {
            list.add( (Integer) node2 );
        }
    }

    /**
     * The method removes an edge from the digraph The source of the edge is
     * labeled node1 The target of the edge is labeled node2
     *
     */
    public void removeEdge( int node1, int node2 )
    {
        if( !data.containsKey( (Integer) node1 ) || !data.containsKey( (Integer) node2 ) )
        {
            return;
        }
        List<Integer> list = data.get( (Integer) node1 );
        if( list.contains( (Integer) node2 ) )
        {
            list.remove( (Integer) node2 );
        }
    }

    /**
     * The method removes a node from the digraph You need to complete this
     * method It should do nothing if the node is not contained in the digraph
     */
    public void remove( int node )
    {
        if( !data.containsKey( (Integer) node ) )
        {
            return;
        }
        List<Integer> list;
        for( Integer i : nodeSet )
        {
            list = data.get( (Integer) i );
            if( list.contains( (Integer) node ) )
            {
                list.remove( (Integer) node );
            }
        }
        data.remove( (Integer) node );
        nodeList.remove( (Integer) node );
    }

    /**
     * This method computes and returns the size (number of edges) of the graph
     */
    public int graphSize()
    {
        List<Integer> list;
        int s = 0;
        for( Integer j : nodeSet )
        {
            list = data.get( (Integer) j );
            s = s + list.size();
        }
        return s;
    }

    /**
     * This method computes and returns the order (number of nodes) of the graph
     */
    public int graphOrder()
    {
        return data.size();
    }

    /**
     * Clear the digraph
     *
     */
    public void clear()
    {
        data.clear();
        nodeList.clear();
    }

    // An inner class storing information regarding the visualisation of a node
    private class Node
    {
        public int nodeNum;

        public Node( int num )
        {
            nodeNum = num;
        }
    }
}