#include <stdlib.h>

#include <cstdio>
#include <iostream>

using namespace std;

typedef struct Node {
    int ID;             // Contém o ID do ponto.
    struct Node *next;  // Apontador para o próximo nó.
    struct Node *prev;  // Apotador para o nó anterior.
} node;

node *NewSentinel() {
    node *aux = (node*)malloc(sizeof(node));
    aux->next = aux->prev = aux;
    return aux;
}

node *NewNode(int ID, node *left, node *right) {
    node *aux = (node*)malloc(sizeof(node));
    aux->ID = ID;
    aux->prev = left;
    aux->next = right;
    return aux;
}

typedef struct {
    node *end;    // Nó marcador do fim da lista.
    int size;     // Número de elementos da lista.
    int color;
} List;

void ListConstructor(List *list) {
    list->end = NewSentinel();
    list->size = 0;
    list->color = 0;
}

node *front(node *listEnd) {
    return listEnd->next;
}

node *back(node *listEnd) {
    return listEnd->prev;
}

int first(node *listEnd) {
    return front(listEnd)->ID;
}

int last(node *listEnd) {
    return back(listEnd)->ID;
}

node *find(int ID, node *listEnd) {
    node *aux;

    for (aux = front(listEnd); aux != listEnd; aux = aux->next) {
        if (ID == aux->ID) {
            return aux;
        }
    }

    return aux;
}

void popFront(List* list) {
    node *aux = front(list->end);

    aux->next->prev = aux->prev;
    aux->prev->next = aux->next;

    free(aux);
    list->size--;
}

void pushBack(int ID, List *list) {
    node *aux = NewNode(ID, back(list->end), list->end);
    back(list->end)->next = aux;
    list->end->prev = aux;
    list->size++;
}

bool empty(int listSize) {
    return( listSize == 0 );
}

void clear(List *list) {
        while (!empty(list->size)) {
                popFront(list);
        }
	free(list->end);
}

void printList(List list) {
    node *n;
    cout << "Size: " << list.size << endl;
    cout << "Color: " << list.color << endl;
    cout << "Adjacentes: ";
    for( n = front(list.end); n != list.end; n = n->next ) {
        cout << n->ID;
        if (n != list.end->prev) {
            cout << " - ";
        }
    }
    cout << endl << endl;
}

List *TableConstructor(int tableSize) {
    List *table = (List*)malloc(sizeof(List)*(tableSize+1));

    for( int i = 1; i <= tableSize; i++ ) {
        ListConstructor(&table[i]);
    }
    return table;
}

void clearTable(List *table, int tableSize) {
    for( int i = 1; i <= tableSize; i++ ) {
        clear(&table[i]);
    }
    free(table);
}

void printTable(List *table, int tableSize) {
    for( int i = 1; i <= tableSize; i++ ) {
        cout << "#Vertice " << i << ": ";
        printList(table[i]);
    }
}

class Graph {
	List *adj;        // Lista de adjacentes de um ponto.
	int numNodes;  // Número de vértices do grafo.
	bool isBipart;
	bool *visited;

	public:
	Graph(int graphSize) {
		adj = TableConstructor(graphSize);
		numNodes = graphSize;
		isBipart = true;
	}

	void addEdge(int ID1, int ID2) {
		pushBack(ID2, &adj[ID1]);
		pushBack(ID1, &adj[ID2]);
	}

	void clear() {
		clearTable(adj, numNodes);
	}

	void print() {
		cout << "Numero de vertices: " << numNodes << endl;
		printTable(adj, numNodes);
	}

	void performDFS( int src ) {
		visited = new bool[numNodes+1];

		for( int i = 1; i <= numNodes; i++ ) {
			visited[i] = false;
		}

		adj[src].color = 1;

		recursiveDFS(src);

		delete[] visited;
	}

	void recursiveDFS( int src ) {
		visited[src] = true;
		int srcColor = adj[src].color;

		for( node *n = front(adj[src].end); n != adj[src].end; n = n->next) {
			int adjColor = adj[n->ID].color;

			if( srcColor == adjColor ) {
				isBipart = false;
			}
			else if( !visited[n->ID] ) {
				if( srcColor == 1 ) {
					adj[n->ID].color = 2;
				}
				else {
					adj[n->ID].color = 1;
				}
				recursiveDFS(n->ID);
			}
		}
	}

	void checkBipart() {
		performDFS(1);

		if( !isBipart ) {
			cout << "nao";
		}
		else {
			cout << "sim";
		}
		cout << endl << endl;
	}
};

int main() {
	int N, M, tCount = 1;

	cin >> N >> M;

	while( !cin.eof() ) {
		Graph graph(N);

		for( int i = 0; i < M; i++ ) {
			int n1, n2;
			cin >> n1 >> n2;
			graph.addEdge( n1, n2 );
		}

		cout << "Instancia " << tCount << endl;
		graph.checkBipart();

		graph.clear();
		tCount++;

		if( !cin.eof() ) {
			cin >> N >> M;
		}
	}

	return 0;
}
