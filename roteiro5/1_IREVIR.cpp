#include <cstdio>
#include <iostream>

using namespace std;

class Graph {
	bool **adj;        // Lista de adjacentes de um ponto.
	int numNodes;  // Número de vértices do grafo.
	bool *visited;

	public:
	Graph(int graphSize) {
		numNodes = graphSize;
		adj = tableConstructor();
	}

	bool **tableConstructor() {
		bool **m = new bool*[numNodes+1];

		for( int i = 1; i <= numNodes; i++ ) {
			m[i] = new bool[numNodes+1];

			for( int j = 1; j <= numNodes; j++ ) {
				m[i][j] = false;
			}
		}

		return m;
	}

	void addEdge(int n1, int n2, int direction) {
		adj[n1][n2] = true;

		if( direction == 2 ) {
			adj[n2][n1] = true;
		}
	}

	void clear() {
		for( int i = 1; i <= numNodes; i++ ) {
			delete[] adj[i];
		}

		delete[] adj;
	}

	void print() {
		cout << "Numero de vertices: " << numNodes << endl;

		for( int i = 1; i <= numNodes; i++ ) {
			cout << "Vertice " << i << ":";

			for( int j = 1; j <= numNodes; j++ ) {
				if( adj[i][j] ) {
					cout << " " << j;
				}
			}

			cout << endl;
		}
	}

	bool performDFS( int src ) {
		visited = new bool[numNodes+1];

		for( int i = 1; i <= numNodes; i++ ) {
			visited[i] = false;
		}

		recursiveDFS(src);

		for( int i = 1; i <= numNodes; i++ ) {
			if( !visited[i] ) {
				delete[] visited;
				return false;
			}
		}

		delete[] visited;
		return true;
	}

	void recursiveDFS( int src ) {
		visited[src] = true;

		for( int i = 1; i <= numNodes; i++ ) {
			if( adj[src][i] && !visited[i] ) {
				recursiveDFS(i);
			}
		}
	}

	void transpose() {
		bool **adj_tmp = tableConstructor();

		for( int i = 1; i <= numNodes; i++ ) {
			for( int j = 1; j <= numNodes; j++ ) {
				if( adj[i][j] ) {
					adj_tmp[j][i] = true;
				}
			}			
		}

		adj = adj_tmp;
	}

	void checkPath() {
		bool result = performDFS(1);

		if( result ) {
			transpose();
			result = performDFS(1);
		}

		if( result ) {
			cout << "1" << endl;
		}
		else {
			cout << "0" << endl;
		}
	}
};

int main() {
	int N,M;

	cin >> N >> M;

	while( N != 0 ) {
		Graph graph(N);

		for( int i = 0; i < M; i++ ) {
			int n1, n2, direction;
			cin >> n1 >> n2 >> direction;

			graph.addEdge( n1, n2, direction );
		}

		graph.checkPath();

		graph.clear();
		cin >> N >> M;
	}

	return 0;
}
