#include <cstdio>
#include <iostream>

using namespace std;

class Duende {
	int N, M;
	int dist;
	bool visited[10][10];

	public:

	int m[10][10];

	Duende( int N, int M ) {
		this->N = N;
		this->M = M;
		dist = 1000000000;
	}

	bool insideMat(int i, int j) {
		return( (i >= 0) && (i < N) && 
			(j >= 0) && (j < M) );
	}

	bool isCrystal( int i, int j ) {
		return( m[i][j] == 2 );
	}

	bool isExit( int i, int j ) {
		return( m[i][j] == 0 );
	}

	bool legalMove( int i, int j ) {
		return( insideMat(i,j) && !isCrystal(i,j)
			&& !visited[i][j] );
	}

	void performDFS( int startX, int startY ) {
		for( int i = 0; i < N; i++ ) {
			for( int j = 0; j < M; j++ ) {
				visited[i][j] = false;
			}
		}

		recursiveDFS( startX, startY, 0 );

		cout << dist << endl;
	}

	void recursiveDFS( int i, int j, int d ) {
		if( legalMove(i,j) ) {
			if( isExit(i,j) ) {
				if( d < dist ) {
					dist = d;
				}
			}
			else {
				visited[i][j] = true;
				d++;

				recursiveDFS( i-1, j, d );
				recursiveDFS( i, j-1, d );
				recursiveDFS( i+1, j, d );
				recursiveDFS( i, j+1, d );
			}
		}
	}
};

int main()
{
	int N, M;
	int startX, startY;

	cin >> N >> M;

	Duende duende(N,M);

	for( int i = 0; i < N; i++ ) {
		for( int j = 0; j < M; j++ ) {
			cin >> duende.m[i][j];

			if( duende.m[i][j] == 3 ) {
				startX = i;
				startY = j;
			}
		}
	}

	duende.performDFS( startX, startY );

        return 0;
}
