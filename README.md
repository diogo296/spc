# Solução de Problemas Computacionais #

* Formato dos arquivos: [numero_lista]_[nome_problema] (ex: 1_COPA1.cpp)
* Roteiros faltando: roteiro 2 (bem fácil) e roteiro 12 (difícil)
* Nem todos os roteiros estão completos e alguns códigos estão com saída correta
* Nota final com estes roteiros: 90
* Dica: os códigos devem ser escritos em C ou C++ por questão de tempo limite do SPOJ