#include <iostream>
#include <cmath>
 
using namespace std;
 
int main() {
	int L, C, R1, R2;

	while( (cin >> L >> C >> R1 >> R2) && (L != 0) ) {
		if( (L < 2*R1) || (L < 2*R2) || (C < 2*R1) || (C < 2*R2) ) {
			cout << "N" << endl;
}
		else {
			double d1 = R1 + R2;
			double d2 = hypotl( (L-R1-R2), (C-R1-R2) );

			if( d2 < d1 ) {
				cout << "N" << endl;
			}
			else {
				cout << "S" << endl;
			}
		}
	}

        return 0;
}
