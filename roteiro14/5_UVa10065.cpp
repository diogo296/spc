#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;
 
struct Point {
	int x, y;
 
	bool operator <(const Point &p) const {
		return x < p.x || (x == p.x && y < p.y);
	}
};
 
double cross(const Point &O, const Point &A, const Point &B)
{
	return (long)(A.x - O.x) * (B.y - O.y) - (long)(A.y - O.y) * (B.x - O.x);
}
 
vector<Point> convexHull(vector<Point> P)
{
	int n = P.size(), k = 0;
	vector<Point> H(2*n);
 
	sort( P.begin(), P.end() );
 
	for( int i = 0; i < n; ++i ) {
		while( k >= 2 && cross(H[k-2], H[k-1], P[i]) <= 0 ) {
			 k--;
		}

		H[k++] = P[i];
	}
 
	for( int i = n-2, t = k+1; i >= 0; i-- ) {
		while( k >= t && cross(H[k-2], H[k-1], P[i]) <= 0 ) {
			 k--;
		}

		H[k++] = P[i];
	}
 
	H.resize(k);

	return H;
}

double area( vector<Point> P ) {
	double area = 0;

	for( int i = 0; i < P.size(); i++) {
		int j = (i+1) % P.size();
		area += (P[i].x + P[j].x) * (P[i].y - P[j].y);
	}

	return abs(area / 2.0);
}

int main() {
	int N, i = 0;

	while( (cin >> N) && (N != 0) ) {
		vector<Point> tile;

		for( ; N > 0; N-- ) {
			Point p;
			cin >> p.x >> p.y;

			tile.push_back(p);
		}

		vector<Point> hull = convexHull(tile);
		double areaHull = area(hull);
		double areaTile = area(tile);
		double wastedSpace = (double) 100 * (1 - areaTile/areaHull);
		i++;

		cout << "Tile #" << i << endl;
		cout << "Wasted Space = ";
		cout << fixed << setprecision(2) << wastedSpace << " %" << endl << endl;
	}

        return 0;
}
