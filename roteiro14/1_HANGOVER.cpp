#include <iostream>
 
using namespace std;
 
int main() {
	double c;

	while( (cin >> c) && (c != 0) ) {
		int nCards = 1;

		for( double sum = 0; sum < c; nCards++ ) {
			sum += (double) 1 / (nCards + 1);
		}

		cout << nCards-1 << " card(s)" << endl;
	}

        return 0;
}
