#include <iostream>
 
using namespace std;

class Line {
	public:

	int x1, y1;
	int x2, y2;

	Line(){}

	void orderCoords() {
		if( x2 < x1 ) {
			int aux = x1;
			x1 = x2;
			x2 = aux;

			aux = y2;
		}
	}
};


int main() {
	int T;

	for( cin >> T; T > 0; T-- ) {
		Line l1, l2;

		cin >> l1.x1 >> l1.y1 >> l1.x2 >> l1.y2;
		cin >> l2.x1 >> l2.y1 >> l2.x2 >> l2.y2;

		if( ((l1.x1 - l1.x2)*(l2.y1 - l2.y2) - (l1.y1 - l1.y2)*(l2.x1 - l2.x2)) == 0 ) {
			cout << "NO" << endl;
		}
		else {
			cout << "ok" << endl;
		}
	}

        return 0;
}
