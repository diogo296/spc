#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
 
struct Point {
	int x, y;
 
	bool operator <(const Point &p) const {
		return x < p.x || (x == p.x && y < p.y);
	}

	bool operator ==(const Point &p) const {
		return( (x == p.x) && (y == p.y) );
	}
};
 
double cross(const Point &O, const Point &A, const Point &B)
{
	return (long)(A.x - O.x) * (B.y - O.y) - (long)(A.y - O.y) * (B.x - O.x);
}
 
vector<Point> convexHull(vector<Point> P)
{
	int n = P.size(), k = 0;
	vector<Point> H(2*n);
 
	sort( P.begin(), P.end() );
 
	for( int i = 0; i < n; ++i ) {
		while( k >= 2 && cross(H[k-2], H[k-1], P[i]) <= 0 ) {
			 k--;
		}

		H[k++] = P[i];
	}
 
	for( int i = n-2, t = k+1; i >= 0; i-- ) {
		while( k >= t && cross(H[k-2], H[k-1], P[i]) <= 0 ) {
			 k--;
		}

		H[k++] = P[i];
	}
 
	H.resize(k);

	return H;
}

int main() {
	int N;

	while( (cin >> N) && (N != 0) ) {
		vector<Point> points;

		for( int i = 0; i < N; i++ ) {
			Point p;
			cin >> p.x >> p.y;

			points.push_back(p);
		}

		int layers = 0;

		while( points.size() > 2 ) {
			vector<Point> hull = convexHull(points);

			for( int i = 0; i < hull.size(); i++ ) {
				for( int j = 0; j < points.size(); j++ ) {
					if( hull[i] == points[j] ) {
						points.erase( points.begin() + j );
						j--;
					}
				}
			}

			hull.clear();
			layers++;
		}

		if( layers % 2 != 0 ) {
			cout << "Take this onion to the lab!" << endl;
		}
		else {
			cout << "Do not take this onion to the lab!" << endl;
		}

		points.clear();
	}

        return 0;
}
