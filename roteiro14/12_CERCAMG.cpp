#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;
 
struct Point {
	int x, y;
 
	bool operator <(const Point &p) const {
		return x < p.x || (x == p.x && y < p.y);
	}
};
 
double cross(const Point &O, const Point &A, const Point &B)
{
	return (long)(A.x - O.x) * (B.y - O.y) - (long)(A.y - O.y) * (B.x - O.x);
}
 
vector<Point> convexHull(vector<Point> P)
{
	int n = P.size(), k = 0;
	vector<Point> H(2*n);
 
	sort( P.begin(), P.end() );
 
	for( int i = 0; i < n; ++i ) {
		while( k >= 2 && cross(H[k-2], H[k-1], P[i]) <= 0 ) {
			 k--;
		}

		H[k++] = P[i];
	}
 
	for( int i = n-2, t = k+1; i >= 0; i-- ) {
		while( k >= t && cross(H[k-2], H[k-1], P[i]) <= 0 ) {
			 k--;
		}

		H[k++] = P[i];
	}
 
	H.resize(k);

	return H;
}

int main() {
	int A, D;

	while( cin >> A >> D ) {
		vector<Point> points;

		for( ; A > 0; A-- ) {
			Point p;
			cin >> p.x >> p.y;

			points.push_back(p);
		}

		vector<Point> fence = convexHull(points);

		double sum = 0;

		for( int i = 1; i < fence.size(); i++ ) {
			Point p1 = fence[i], p2 = fence[i-1];

			sum += hypotl( (p1.x - p2.x), (p1.y - p2.y) );
		}

		cout << fixed << setprecision(2) << sum << endl;
	}

        return 0;
}
