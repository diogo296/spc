#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
        int N, dif;
        int total = 0;

        cin >> N;

        switch(N)
        {
                case 101 ... 1000:
                        dif = N - 100;
                        total += dif * 5;

                        N -= dif;
                case 31 ... 100:
                        dif = 100 - N;
                        total += (70 - dif) * 2;

                        N -= (70 - dif);
                case 11 ... 30:
                        total += N - 10;
                default:
                        total += 7;
        }

        cout << total << endl;

        return 0;
}