#include <stdio.h>
#include <stdlib.h>
 
int main()
{
        int fib[47];
        int i, n;
 
        scanf( "%d", &n );
 
        fib[1] = 0;
        fib[2] = 1;
 
        printf( "%d ", fib[1] );
        printf( "%d", fib[2] );
 
        for( i = 3; i <= n; i++ )
        {
                fib[i] = fib[i-1] + fib[i-2];
                printf( " %d", fib[i] );
        }
 
        printf("\n");
 
        return EXIT_SUCCESS;
}
