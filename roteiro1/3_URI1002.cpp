#include <cstdio>                                                    
#include <iostream>                                                  
                                                                      
using namespace std;                                                 
                                                                      
int main()                                                           
{                                                                    
        double R;                                                    
        double PI = 3.14159;                                         
                                                                      
        cin >> R;                                                    
                                                                      
        cout.precision(4);                                           
        cout << "A=" << fixed << PI * R * R << endl;                             
                                                                      
        return 0;                                                    
}