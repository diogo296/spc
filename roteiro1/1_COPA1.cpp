#include <cstdio>
#include <iostream>

using namespace std;

string evaluateRound( int numTeams, string teams )
{
	int i;
	int t1, t2;
	string winners = "";

	for( i = 0; i < numTeams; i += 2 )
	{
		scanf( "%d", &t1 );
		scanf( "%d", &t2 );

		if( t1 > t2 )
		{
			winners += teams[i];
		}
		else
		{
			winners += teams[i+1];
		}
	}

	return winners;
}

int main()
{
	string teams = "ABCDEFGHIJKLMNOP";

	teams =	evaluateRound( 16, teams);
	teams =	evaluateRound( 8, teams);
	teams =	evaluateRound( 4, teams);
	teams =	evaluateRound( 2, teams);

	cout << teams;
	cout << endl;

        return 0;
}