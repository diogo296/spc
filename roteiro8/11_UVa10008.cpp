#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <vector>

using namespace std;

bool comp( pair<char, int> a, pair<char, int> b ) {
	if( a.second == b.second ) {
		return a.first < b.first;
	}
	else {
		return a.second > b.second;
	}
}

int main() {
	int N;
	map<char, int> count;

	for( cin >> N; N >= 0; N-- ) {
		string str;
		getline(cin, str);

		for( int i = 0; i < str.length(); i++ ) {
			char c = toupper(str[i]);

			if( (c >= 'A') && (c <= 'Z') ) {
				if( count.find(c) == count.end() ) {
					count[c] = 1;
				}
				else {
					count[c]++;
				}
			}
		}
	}

	vector<pair<char, int> > v;

	for( map<char, int>::iterator it = count.begin(); it != count.end(); it++ ) {
		v.push_back( make_pair(it->first, it->second) );
	}

	sort( v.begin(), v.end(), comp );

	for( int i = 0; i < v.size(); i++ ) {
		cout << v[i].first << " " << v[i].second << endl;
	}

	return 0;
}
