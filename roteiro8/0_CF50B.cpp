#include <iostream>
#include <string>
#include <map>

using namespace std;

int main() {
	string str;
	cin >> str;

	map<char, long long int> letters;
	long long int total = str.length();

	for( int i = 0; i < total; i++ ) {
		if( letters.find(str[i]) == letters.end() ) {
			letters[ str[i] ] = 1;
		}
		else {
			letters[ str[i] ]++;
		}
	}

	for( map<char, long long int>::iterator it = letters.begin(); it != letters.end(); it++ ) {
		long long int val = it->second;
		total += val*val - val;
	}

	cout << total << endl;

	return 0;
}
