#include <iostream>
#include <string>

using namespace std;

int main() {
	string tel;
	string val = "22233344455566677778889999";

	while( cin >> tel ) {
		string telNum = "";

		for( int i = 0; i < tel.length(); i++ ) {
			if( (tel[i] == '0') || (tel[i] == '1') || (tel[i] == '-') ) {
				telNum += tel[i];
			}
			else {
				telNum += val[ tel[i] - 'A' ];
			}
		}

		cout << telNum << endl;
	}

	return 0;
}
