#include <iostream>
#include <string>

using namespace std;

int main() {
	double N, P;
	int min = 6, max = 20;
	int maxOs = max - min;

	while( (cin >> N >> P) && (N != 0) ) {
		int numPags = ceil(N / P);
		int numOs = numPags - min;
		string poodle = "";

		if( numPags > min ) {
			while( (poodle.length() < numOs ) && (poodle.length() < maxOs) ) {
				poodle += "o";
			}
		}

		cout << "Poo" << poodle << "dle" << endl;
	}

	return 0;
}
