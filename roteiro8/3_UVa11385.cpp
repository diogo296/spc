#include <iostream>
#include <vector>
#include <cmath>
#include <string>

#define LL unsigned long long int
#define MAX 100

using namespace std;

class Fibonacci {
	private:
	LL fib[MAX];

	void calcFib() {
		fib[0] = 1;
		fib[1] = 2;

		for( int i = 2; i < MAX; i++ ) {
			fib[i] = fib[i-1] + fib[i-2];
		}
	}

	public:
	Fibonacci() {
		calcFib();
	}

	int findIndex( LL n ) {
		for( int i = 0; i < n; i++ ) {
			if( fib[i] == n ) {
				return i;
			}
		}
	}
};

int main() {
        int T;
	Fibonacci fb;

        for( cin >> T; T > 0; T-- ) {
                int N;
		LL val, bigger = 0;
		vector<LL> fibNumbers;

		cin >> N;

                for( int i = 0; i < N; i++ ) {
			cin >> val;

			fibNumbers.push_back(val);
			bigger = max( bigger, val );
                }

                string code, decode( fb.findIndex(bigger)+1, ' ' );

		getline(cin, code);
		getline(cin, code);

		// Remove non-alpha from code
		for( int i = 0; i < code.size(); i++ ) {
			if( ('A' > code[i]) || ('Z' < code[i]) ) {
				code.erase( code.begin()+i );
				i--;
			}

		}

		// Decode
		for( int i = 0; i < code.size(); i++ ) {
			int j = fb.findIndex( fibNumbers[i] );
			decode[j] = code[i];
		}

		cout << decode << endl;
        }

        return 0;
}
