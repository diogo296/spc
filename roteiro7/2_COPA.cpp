#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

int main()
{
	int T, N;

	while( (cin >> T >> N) && (T != 0) ) {
		int maxPoints = 3 * N;
		int sum = 0;
		int result;
		string teamName;

		for( int i = 0; i < T; i++ ) {
			cin >> teamName >> result;
			sum += result;
		}

		cout << (maxPoints - sum) << endl;
	}

        return 0;
}
