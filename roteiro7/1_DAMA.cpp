#include <cstdio>
#include <iostream>
#include <cmath>

using namespace std;

int main() 
{
	int x1, y1, x2, y2;
	int numMoves;

	while( (cin >> x1 >> y1 >> x2 >> y2) && (x1 != 0) ) {
		if( (x1 == x2) && (y1 == y2) ) {
			numMoves = 0;
		}
		else if( (x1 == x2) || (y1 == y2) || (abs(x1-x2) == abs(y1-y2)) ) {
			numMoves = 1;
		}
		else {
			numMoves = 2;
		}

		cout << numMoves << endl;
	}


        return 0;
}
