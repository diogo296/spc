#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
	int N, birth, death;

	for( cin >> N; N > 0; N-- ) {
		cin >> birth >> death;

		int age = death - birth;

		if( (death > 0) && (birth < 0) ) {
			age--;
		}

		cout << age << endl;
	}

        return 0;
}
