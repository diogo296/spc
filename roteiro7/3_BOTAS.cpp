#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	const int MAX = 31;
	int N;

	while( cin >> N ) {
		int E[MAX], D[MAX];

		for( int i = 0; i < MAX; i++ ) {
			D[i] = 0;
			E[i] = 0;
		}

		int M;
		char L;

		for( int i = 0; i < N; i++ ) {
			cin >> M >> L;

			if( L == 'D' ) {
				D[M-30]++;
			}
			else {
				E[M-30]++;
			}
		}

		int total = 0;

		for( int i = 0; i < MAX; i++ ) {
			total += min( E[i], D[i] );
		}

		cout << total << endl;
	}

        return 0;
}
