#include <cstdio>
#include <iostream>
#include <cmath>

using namespace std;

int split( double N, double K ) {
	if( N <= K ) {
		return 1;
	}
	else {
		return( split(ceil(N/2), K) + split(floor(N/2), K) );
	}
}

int main()
{
	double N, K;

	while( (cin >> N >> K) && (N != 0) ) {
		cout << split(N, K) << endl;
	}

        return 0;
}
