#include <iostream>
#include <cmath>
#include <iomanip>

#define RAD M_PI/180

using namespace std;

int main() {
        double x1, y1, x2, y2, CMD, ENF;

        while( cin >> x1 >> y1 >> x2 >> y2 >> CMD >> ENF ) {
                double dist = hypotl( x1-x2, y1-y2 ) * 
			( (1/tan(CMD * RAD)) + (1/tan(ENF * RAD)) );

                cout << fixed << setprecision(3) << dist << endl;
        }

        return 0;
}
