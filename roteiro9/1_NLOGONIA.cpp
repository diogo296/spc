#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

int main()
{
        int K;

        while( (cin >> K) && (K != 0) ) {
                int dx, dy, x, y;

                cin >> dx >> dy;

                for( ; K > 0; K-- ) {
                        string country;

                        cin >> x >> y;

                        if( (x < dx) && (y < dy) ) {
                                country = "SO";
                        }
                        else if( (x < dx) && (y > dy) ) {
                                country = "NO";
                        }
                        else if( (x > dx) && (y < dy) ) {
                                country = "SE";
                        }
                        else if( (x > dx) && (y > dy) ) {
                                country = "NE";
                        }
                        else {
                                country = "divisa";
                        }

                        cout << country << endl;
                }
        }

        return 0;
}
