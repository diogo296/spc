#include <cstdio>
#include <iostream>

using namespace std;

int main() {
        int N;
        int x1, y1, x2, y2;

        for( int i = 1; (cin >> x1 >> y1 >> x2 >> y2) &&
               ((x1 != 0) && (y1 != 0) && (x2 != 0) && (y2 != 0)); i++ ) {
                int N, mx, my, total = 0;

                cin >> N;

                for( ; N > 0; N-- ) {
                        cin >> mx >> my;

                        if( (mx >= x1) && (mx <= x2) &&
                            (my <= y1) && (my >= y2) ) {
                                total++;
                        }
                }

                cout << "Teste " << i << endl;
                cout << total << endl << endl;
        }

        return 0;
}
