#include <cstdio>
#include <iostream>
#include <cmath>
#include <vector>
#include <climits>
#include <iomanip>

using namespace std;

class Point {
        public:
        long int x, y;

        Point() {}

        Point( int x, int y ) {
                this->x = x;
                this->y = y;
        }

        long double dist( Point p ) {
                return hypotl( x - p.x, y - p.y );
        }
};

int main()
{
        int N;
        long int x, y;

        cin >> N;

        vector<Point> points;

        for( int i = 0; i < N; i++ ) {
                cin >> x >> y;
                Point point(x,y);

                points.push_back( point );
        }

        long double lower = INT_MAX;

        if( points.size() < 2 ) {
                lower = 0;
        }
        else {
                for( int i = 0; i < points.size(); i++ ) {
                        for( int j = i+1; j < points.size(); j++ ) {
                                if( lower > points[i].dist(points[j]) ) {
                                        lower = points[i].dist(points[j]);
                                }
                        }
                }
        }

        cout << fixed << setprecision(3) << lower << endl;

        return 0;
}
