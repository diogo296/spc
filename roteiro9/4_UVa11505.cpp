#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>

#define RAD M_PI/180

using namespace std;

int main() {
        int N;

        for( cin >> N; N > 0; N-- ) {
                int M, steps;
                double x = 0, y = 0, angle = 0;
                string command;

                for( cin >> M; M > 0; M-- ) {
                        int direction = 1;
                        cin >> command;

                        if( (command == "bk") || (command == "rt") ) {
                                direction = -1;
                        }

                        if( (command == "lt") || (command == "rt") ) {
                                int angle_tmp;
                                cin >> angle_tmp;

                                angle += angle_tmp * direction;
                        }
                        else {
                                cin >> steps;

                                x += direction * steps * cos(angle * RAD);
                                y += direction * steps * sin(angle * RAD);
                        }

                }

		cout << fixed << setprecision(0) << hypotl(x, y) << endl;
        }

        return 0;
}
